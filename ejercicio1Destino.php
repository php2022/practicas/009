<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            table,td{
                border: 1px solid black;
                border-collapse: collapse;
            }
            td{
                padding: 10px;
                    width:100px;
            }
        </style>
    </head>
    <body>
        <table>
            <tr>
                <td>Nombre</td>
                <td><?= $_POST["nombre"] ?></td>
            </tr>
            <tr>
                <td>Color</td>
                <td><?= $_POST["color"] ?></td>
            </tr>
            <tr>
                <td>Alto</td>
                <td><?= $_POST["alto"] ?></td>
            </tr>
            <tr>
                <td>Peso</td>
                <td><?= $_POST["peso"] ?></td>
            </tr>
        </table>
    </body>
</html>
